from flask import Flask, make_response, render_template , redirect, url_for , flash
from flask_login import login_user, LoginManager, UserMixin
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField 
from wtforms.validators import Length , DataRequired , ValidationError


app = Flask(__name__ , template_folder='template',static_folder= 'static')
app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///user.db'
app.config['SECRET_KEY'] = '5c4e5d3b0f213d25a93779a6'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
bcrypt=Bcrypt(app)
login_manager = LoginManager(app)

# loginform.py
class LoginForm(FlaskForm):
  def validate_username(self,username_to_check) :
    user= User.query.filter_by(Username=username_to_check.data).first()
    if user:
            raise ValidationError('Username already exists! Please try a different username')
        
  Username = StringField(validators=[Length(min=2 , max =30), DataRequired() ])
  Password1 = PasswordField(validators=[Length(min=2 , max =30), DataRequired()]  )
  Login = SubmitField()

# models.py
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class User(db.Model , UserMixin):
    ID = db.Column(db.Integer() , primary_key=True)
    Username = db.Column(db.String(length=50) , nullable=False , unique=True)
    Password_hash = db.Column(db.String(length=20) , nullable=False)
    
    
@property    
def password(self):
    return self.Password

@password.setter
def password(self,plain_text_password):
    self.Password_hash=bcrypt.generate_password_hash(plain_text_password).decode('utf-8')
    
def check_password_correction(self, attempted_password):
        return bcrypt.check_password_hash(self.password_hash, attempted_password)    

@app.route('/Login',methods =['GET','POST'])
def Login():
    print('Login')
    form = LoginForm()
    if form.validate_on_submit():
        attempted_user = User.query.filter_by(Username=form.Username.data).first()
        if attempted_user and attempted_user.check_password_correction( attempted_password=form.password.data ):
            login_user(attempted_user)
            flash(f'Success! You are logged in as: {attempted_user.Username}', category='success')
            return redirect(url_for('Dashboard'))
        else:
            flash('Username and password are not match! Please try again', category='danger')

    return render_template('signin1.html', LoginForm=form)



@app.route('/Dashboard',methods= ['GET','POST'])
def Dashboard():
    print('Dashboard')
    return render_template('dashboard.html')

@app.route('/Physiotherapy',methods=['GET','POST'])
def Physiotherapy():
    print('Physiotherapy')
    return render_template('physio_form.html')

#Index route
@app.route('/')
def index():
    print('ok')
    return make_response( 
        'Ok',
        200
        )
if __name__ == "__main__":
    app.run(host="0.0.0.0", port="5001", debug=True)